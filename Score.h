#pragma once
#include "User.h"

/**
*Class reprezenting current score of a player
*/
class Score
{
public:
    /**
    *Default constructor
    */
    Score();

    /**
    *Constructor creating game score
    *@param number of used moves
    *@param size of the grid
    *@param player name
    */
    Score(int,int, std::string);

    /**
     * Method used for incrementing used moves
    */
    void incrementNoOfMoves();


    /**
     * Method used for getting used moves count
     * \return number of moves
    */
    int getNumberOfMoves();

    /**
     * Method used for User (player)
     * \return User object
    */
    User& getPlayer();


private:
    int numberOfMoves; /*!< Total count of done movements */
    int size; /*!< Size of the grid */
    User player; /*!< Player associated with the Score */
};

