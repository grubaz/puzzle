#include "User.h"

User::User(){
    name = "unknown player";
}

User::User(std::string s) {
    if(s == "") s = "unknown player";
	name = s;
}

std::string User::getName() const {
	return name;
}
