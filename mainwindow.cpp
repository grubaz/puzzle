#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QLabel>
#include <QFileDialog>


#include<QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_btn_loadImage_clicked()
{
    //image_path = QFileDialog::getOpenFileUrl(this, "Choose an image", QUrl(),tr("JPEG (*.jpg *.jpeg)")).toString();
    QFileDialog dialog2(this);
    dialog2.setNameFilter(tr("JPEG (*.jpg *.jpeg)"));
    dialog2.setDirectory(QDir::currentPath() + "/img");

    if (dialog2.exec()){
        image_path = dialog2.selectedFiles().first();
    }

}

void MainWindow::on_btn_startGame_clicked()
{ 
    int grid_size = this->ui->spinBox_dimensions->value();
    QString username =  this->ui->usernameInput->text();
    game = new GameWindow(this, grid_size, username, image_path);
    game->show();
    this->hide();
}



void MainWindow::on_btn_loadGame_clicked()
{
    game = new GameWindow(this, image_path);
    QFileDialog dialog(this);
    QString path;
    dialog.setNameFilter(tr("txt (*.txt)"));
    dialog.setDirectory(QDir::currentPath() + "/save");

    if (dialog.exec()){
        path = dialog.selectedFiles().first();
        GameSerializer::load(game, path);
        game->show();
        this->hide();
    }
}
