#pragma once
#include "gamewindow.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include <QDebug>

/**
*Static class used for game state serialization
*/
class GameSerializer
{
public:
    /**
     * Static method used for saving actual game state to the file
     * @param pointer to current GameWindow object
     * @param Save file path
    */
    static void save(GameWindow*, QString);

    /**
     * Static method used for loading a game state from the file
     * @param pointer to current GameWindow object
     * @param Save file path
    */
    static void load(GameWindow*, QString);
};

