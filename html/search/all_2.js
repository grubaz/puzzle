var searchData=
[
  ['game_6',['Game',['../class_game.html',1,'Game'],['../class_game.html#ad59df6562a58a614fda24622d3715b65',1,'Game::Game()'],['../class_game.html#aee7fb1838072459102823cf747c07751',1,'Game::Game(int, std::string)'],['../class_game.html#a8b0cffb221521258633cab70d770ead1',1,'Game::Game(int, int, int, std::string, std::vector&lt; int &gt; *)']]],
  ['gameserializer_7',['GameSerializer',['../class_game_serializer.html',1,'']]],
  ['gamewindow_8',['GameWindow',['../class_game_window.html',1,'GameWindow'],['../class_game_window.html#a999bbfbb38f2fdd1a5dedd639dbbe362',1,'GameWindow::GameWindow(QWidget *parengt=nullptr, int grid_size=3, QString username=&quot;unknown player&quot;, QString=QDir::currentPath())'],['../class_game_window.html#ac939aee1c397e24e934db8638372bfcd',1,'GameWindow::GameWindow(QWidget *parengt, QString=QDir::currentPath())']]],
  ['getboard_9',['getBoard',['../class_game.html#a6c017d264046f13121ddf00a952f23ed',1,'Game']]],
  ['getgame_10',['getGame',['../class_game_window.html#a167389f57bcf6ad5e7b1f953974aef30',1,'GameWindow']]],
  ['getimagepath_11',['getImagePath',['../class_game_window.html#aa8bc6ce9b01012f0c5ab7ddba8afff6b',1,'GameWindow']]],
  ['getmove_12',['getMove',['../class_puzzle.html#a9ad701cc9e2ce91a4f28174e4c7bc1fb',1,'Puzzle::getMove()'],['../class_tile.html#ae84dae4af520b8a78616cd3a98433cf8',1,'Tile::getMove()']]],
  ['getname_13',['getName',['../class_user.html#a446a64e63adafbc2e1428532275ad6a1',1,'User']]],
  ['getnumberofmoves_14',['getNumberOfMoves',['../class_game.html#a72a12483baa3707de20ed1b9df5ecb44',1,'Game::getNumberOfMoves()'],['../class_score.html#aa2bc5ca50489526945c991610912b38f',1,'Score::getNumberOfMoves()']]],
  ['getplayer_15',['getPlayer',['../class_score.html#afc85cad21897aeca8d3aa8975c30e9ca',1,'Score']]],
  ['getscore_16',['getScore',['../class_game.html#a96833e42a7ea317cc7151274d8b279f5',1,'Game']]],
  ['getsize_17',['getSize',['../class_game.html#a84250e80d719e6abda68334ce7a63cef',1,'Game']]],
  ['gettile_18',['getTile',['../class_game.html#aeac1f4b494dff96412bbde026b007f8d',1,'Game']]],
  ['getvalue_19',['getValue',['../class_puzzle.html#a9c3c95dffe6618007824b25b844d28e4',1,'Puzzle::getValue()'],['../class_tile.html#a74f4f6ff111e6ab990bae0530eb5fd3b',1,'Tile::getValue()']]]
];
