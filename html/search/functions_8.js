var searchData=
[
  ['save_79',['save',['../class_game_serializer.html#ae2c71f2c0cf12c541976dfec74bec9f4',1,'GameSerializer']]],
  ['score_80',['Score',['../class_score.html#a039c99843551e5e4b512ecee99e46617',1,'Score::Score()'],['../class_score.html#af28e738ac4e0031541029af2a9067085',1,'Score::Score(int, int, std::string)']]],
  ['setempty_81',['setEmpty',['../class_game.html#a4deb7680649aa83d8824ad2d3731b11d',1,'Game']]],
  ['setgame_82',['setGame',['../class_game_window.html#ae1b2b0cb3361b5945d409d670e772816',1,'GameWindow']]],
  ['setgridsize_83',['setGridSize',['../class_game_window.html#a92c6684fbe1f4e0a8f62b85443ba5b2e',1,'GameWindow']]],
  ['setimagepath_84',['setImagePath',['../class_game_window.html#a344c296f30be92d12c1a517dc73300a1',1,'GameWindow']]],
  ['setmove_85',['setMove',['../class_puzzle.html#a8f84699da16ae76dde4a861e0a77c7dc',1,'Puzzle::setMove()'],['../class_tile.html#acdb4e7b65194c7f19e5199b339a8413b',1,'Tile::setMove()']]],
  ['swaptiles_86',['swapTiles',['../class_game.html#ad075c8e3097ff680006d81ec1f0cbcee',1,'Game']]]
];
