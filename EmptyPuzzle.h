#pragma once
#include "Tile.h"

/**
*Class reprezenting movable puzzle - empty one
*/
class EmptyPuzzle :
	public Tile
{
public:
    /**
    *Constructor calling base class constructor
    *@param x postition on the grid
    *@param y postition on the grid
    */
    EmptyPuzzle(int x, int y);

    /**
     * Virtual method from base Tile class used to verify in runtime if tile is empty puzzle
     * \return True
    */
    virtual bool isEmpty() const;
};

