#pragma once

//list of all move directions
enum MOVES { NONE = 0, UP, DOWN, RIGHT, LEFT };


/**
*Interface reprezenting square piece of board
*/
class Tile
{
public:
    /**
    *Constructor creating tile at given position
    *@param x postition on the grid
    *@param y postition on the grid
    */
	Tile(int, int);


    /**
     * Virtual method used to verify in runtime if tile is empty puzzle
     * \return True/False
    */
	virtual bool isEmpty() const;

    /**
     * Virtual method used to get value of a Tile
     * \return False
    */
    virtual int getValue() const;

    /**
     * Virtual method used to get available move of a Tile
     * \return 0
    */
	virtual int getMove() const;

    /**
     * Virtual method used to set available move of a Tile
     * @param direction of move
    */
    virtual void setMove(int);


    int posX; /*!< Position on the X axis */
    int posY; /*!< Position on the Y axis */
};

