#include "gamewindow.h"
#include "ui_gamewindow.h"
#include "mainwindow.h"
#include <QScreen>


#include <QDebug>

GameWindow::GameWindow(QWidget *parent, int size, QString username, QString image_path) :
    QMainWindow(parent),
    ui(new Ui::GameWindow)
{
    ui->setupUi(this);
    game = Game(size, username.toStdString());
    if(image_path == "") this->image_path = QDir::currentPath() + "/img/back.jpg";
    else this->image_path = image_path;
    this->grid_size = size;
    createLabels();
    this->setEnabled(true);
    setTimer();
}

GameWindow::GameWindow(QWidget *parent, QString image_path) :
    QMainWindow(parent),
    ui(new Ui::GameWindow)
{
    ui->setupUi(this);
    if(image_path == "") this->image_path = QDir::currentPath() + "/img/back.jpg";
    else this->image_path = image_path;
    this->setEnabled(true);
    setTimer();
}

GameWindow::~GameWindow()
{
    delete ui;
}

void GameWindow::createLabels(){
    this->imgLabels = std::vector<QLabel*>();
    this->textLabels = std::vector<QLabel*>();


    int posX = init_posX;
    int posY = init_posY;

    puzzle_size = board_size / grid_size;

    for (int i=1;i<=grid_size;i++) {
        for(int j=1;j<=grid_size;j++) {
            QString value = QString::number(game.getTile(i-1,j-1)->getValue());

            if(value == "0"){
                game.emptyX = i-1;
                game.emptyY = j-1;
                QPixmap blackPix(puzzle_size, puzzle_size);
                blackPix.fill(Qt::black);

                QLabel *empty = new QLabel(this);
                empty->setGeometry(posX, posY, puzzle_size, puzzle_size);
                empty->setPixmap(blackPix);
                imgLabels.push_back(empty);

                QLabel *text = new QLabel(this);
                QFont font = text->font();
                font.setPixelSize(puzzle_size/3);
                text->setFont(font);
                text->setGeometry(posX + puzzle_size/3, posY + puzzle_size/3, puzzle_size/3, puzzle_size/3);
                text->setAlignment(Qt::AlignCenter);
                text->setText(value);
                textLabels.push_back(text);

                posX += puzzle_size + offset;
            } else {
                //QPixmap pix(QDir::currentPath() + "/img/back.jpg");
                QPixmap pix(image_path);

                qInfo() << image_path;
                QLabel *label = new QLabel(this);
                label->setGeometry(posX, posY, puzzle_size, puzzle_size);
                label->setPixmap(pix.scaled(puzzle_size, puzzle_size, Qt::KeepAspectRatio));
                imgLabels.push_back(label);

                QLabel *text = new QLabel(this);
                QFont font = text->font();
                font.setPixelSize(puzzle_size/3);
                text->setFont(font);
                text->setGeometry(posX + puzzle_size/3, posY + puzzle_size/3, puzzle_size/3, puzzle_size/3);
                text->setAlignment(Qt::AlignCenter);
                text->setText(value);
                textLabels.push_back(text);

                posX += puzzle_size + offset;
            }


        }
        posX = init_posX;
        posY += puzzle_size + offset;
    }


}

void GameWindow::mousePressEvent(QMouseEvent *e){   
    int y = e->x() / puzzle_size;
    int x = e->y() / puzzle_size;



    if(x >=0 && x < grid_size && y>=0 && y < grid_size){
        if(game.moveTile(x,y)){
            swapLabels(x,y);
            game.setEmpty(x,y);
            game.updateMoves();
            ui->MovesLCD->display(game.getNumberOfMoves());
            if(game.checkOrder()){
                endGame();
            }
        }



    }
}


void GameWindow::swapLabels(int x, int y){
    int firstIndex = grid_size * x + y;
    int secondIndex = grid_size * game.emptyX + game.emptyY;

    int tempX = imgLabels.at(firstIndex)->x();
    int tempY = imgLabels.at(firstIndex)->y();


    imgLabels.at(firstIndex)->setGeometry(
                imgLabels.at(secondIndex)->x(),
                imgLabels.at(secondIndex)->y(),
                imgLabels.at(firstIndex)->width(),
                imgLabels.at(firstIndex)->height());

    imgLabels.at(secondIndex)->setGeometry(
                tempX,
                tempY,
                imgLabels.at(secondIndex)->width(),
                imgLabels.at(secondIndex)->height());

    tempX = textLabels.at(firstIndex)->x();
    tempY = textLabels.at(firstIndex)->y();

    textLabels.at(firstIndex)->setGeometry(
                textLabels.at(secondIndex)->x(),
                textLabels.at(secondIndex)->y(),
                textLabels.at(firstIndex)->width(),
                textLabels.at(firstIndex)->height());

    textLabels.at(secondIndex)->setGeometry(
                tempX,
                tempY,
                textLabels.at(secondIndex)->width(),
                textLabels.at(secondIndex)->height());

    iter_swap(textLabels.begin() + firstIndex, textLabels.begin() + secondIndex);
    iter_swap(imgLabels.begin() + firstIndex, imgLabels.begin() + secondIndex);

    for(auto el : imgLabels) el->lower();
    for(auto el : textLabels) el->raise();
}

void GameWindow::endGame(){
    QString str("Congratulations! \nYou've ended game in ");
    str += QString::number(game.elapsedSecs) + " seconds and " + QString::number(game.getNumberOfMoves()) + " moves";
    QMessageBox* msg = new QMessageBox(this);
    msg->setText(str);

    msg->show();
    this->hide();
    this->parentWidget()->show();

}

QString GameWindow::updateTime(){
    getGame().elapsedSecs++;
    auto tmp = getGame().elapsedSecs;
    this->ui->TimeLCD->display(QDateTime::fromTime_t(tmp).toUTC().toString("hh:mm:ss"));
    return QDateTime::fromTime_t(tmp).toUTC().toString("hh:mm:ss");
}
void GameWindow::setTimer(){

    elapsed = new QElapsedTimer();
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateTime()));
    elapsed->start();
    timer->start(1000);
}


void GameWindow::on_pushButton_clicked(){


    bool ok;
    timer->stop();

    QString text = QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                          tr("Name of save:"), QLineEdit::Normal,
                                          "my_saved_game", &ok);
    if (ok && !text.isEmpty()){
        GameSerializer::save(this, text);
    }
    elapsed->restart();
    timer->start(1000);
}

void GameWindow::setGame(Game& g){
    game = g;
}

Game& GameWindow::getGame(){
    return game;
}

void GameWindow::setGridSize(int size){
    grid_size = size;
}

QString GameWindow::getImagePath(){
    return image_path;
}

void GameWindow::setImagePath(QString path){
    image_path = path;
}

void GameWindow::refreshLCD(){
    ui->MovesLCD->display(game.getScore().getNumberOfMoves());
    ui->TimeLCD->display(game.elapsedSecs);
}


