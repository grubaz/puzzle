#include "Score.h"


Score::Score(){
    numberOfMoves = 0;
    size = 3;
    player = User();
}

Score::Score(int n, int s, std::string p) {
	numberOfMoves = n;
	size = s;
	player = User(p);
}

void Score::incrementNoOfMoves(){
    numberOfMoves++;
}

int Score::getNumberOfMoves(){
    return numberOfMoves;
}

User& Score::getPlayer(){
    return player;
}
