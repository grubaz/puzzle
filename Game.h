#pragma once
#include "User.h"
#include "Tile.h"
#include "Puzzle.h"
#include "EmptyPuzzle.h"
#include <vector>
#include <algorithm>
#include <time.h>
#include "Score.h"

/**
*Class reprezenting and managing current game state
*/
class Game
{
public:
    /**
     * Default constructor
    */
    Game();

    /**
     * Constructor creating the game with size and user
     * @param size of board
     * @param username
    */
    Game(int, std::string);

    /**
     * Constructor creating the game with specific board state (used to load game)
     * @param size of board
     * @param elapsed time in seconds
     * @param number of done moves
     * @param username
     * @param vector containing ordered values for Tiles
    */
    Game(int, int, int, std::string, std::vector<int>*);

    /**
     * Method for fetching specific Tile
     * @param tile position X
     * @param tile position Y
     * \return Tile at position (X,Y)
    */
    Tile* getTile(int, int);

    /**
     * Method for checking if a Tile can be moved
     * @param tile position X
     * @param tile position Y
     * \return True/False
    */
	bool moveTile(int, int);

    /**
     * Method for swapping Two tiles places
     * @param First Tile
     * @param Second Tile
    */
    void swapTiles(Tile*, Tile*);

    /**
     * Method for checking current board order.
     * Used for checking if game is finished
     * \return True/False
    */
	bool checkOrder();

    /**
     * Method for updating available move direction for all board Tiles
    */
	void updateMoves();

    /**
     * Method for randomize board state in new game
     * @param number of random moves
    */
    void randomizeBoard(int);

    /**
     * Method for setting EmptyTile position
     * @param EmptyTile position X
     * @param EmptyTile position Y
    */
    void setEmpty(int, int);

    /**
     * Method for getting number of done moves
     * \return number of done moves
    */
    int getNumberOfMoves();

    /**
     * Method for getting board size
     * \return board size
    */
    int getSize();

    /**
     * Method for getting score
     * \return Score object
    */
    Score& getScore();


    /**
     * Method for getting game board
     * \return vector of Tiles pointers
    */
    std::vector<Tile*> getBoard();

    int emptyX; /*!< EmptyTile psition on the X axis */
    int emptyY; /*!< EmptyTile psition on the Y axis */
    int elapsedSecs = 0; /*!< Elapsed time in secoinds */
	
private:
    std::vector<Tile*> board; /*!< Vector of Tiles pointers */
    int size; /*!< size of the board */
    Score score; /*!< Current score */
};

