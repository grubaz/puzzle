#include "Game.h"

Game::Game(){
    size = 3;
    score = Score();
}

Game::Game(int size, std::string username) {
    this->size = size;
    score = Score(0, size, username);
    emptyX = size - 1;
    emptyY = size - 1;
	
	int value = 1;

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
            board.push_back(new Puzzle(i, j, value));
			value++;
		}
	}

	board.pop_back();
    board.push_back(new EmptyPuzzle(size - 1, size - 1));
    randomizeBoard(100); //SHUFFLE TILES IN NEW GAME
    updateMoves();
}

Game::Game(int size, int time, int moves, std::string username, std::vector<int>* values) {
    this->size = size;
    elapsedSecs = time;
    score = Score(moves, size, username);
    //emptyX = size - 1;
    //emptyY = size - 1;

    int value = 0;

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            value = values->at(size * i + j);
            if(value == 0) {
                emptyX = i;
                emptyY = j;
                board.push_back(new EmptyPuzzle(i,j));
            }
            else board.push_back(new Puzzle(i, j, value));
            value++;
        }
    }
    updateMoves();

}

Tile* Game::getTile(int x, int y) {
	return board.at(x * size + y);
}


bool Game::moveTile(int x, int y) {

    Tile* clicked = getTile(x, y);
    int direction = clicked->getMove();

	//enum order NONE, UP, DOWN, RIGHT, LEFT
    if (direction != NONE && !clicked->isEmpty()) {
		switch (direction){
        case UP:
			Game::swapTiles(clicked, getTile(x - 1, y));           
			break;
		case DOWN:
            Game::swapTiles(clicked, getTile(x + 1, y));
			break;
		case RIGHT:
            Game::swapTiles(clicked, getTile(x , y + 1));
			break;
		case LEFT:
            Game::swapTiles(clicked, getTile(x, y - 1));
			break;
		default:
			break;
		}
        score.incrementNoOfMoves();
        return true;
	}
    return false;

}

void Game::swapTiles(Tile* t1, Tile* t2) {

    int firstIndex = size * t1->posX + t1->posY;
    int secondIndex = size * t2->posX + t2->posY;

    int tempX = t1->posX;
    int tempY = t1->posY;

    t1->posX = t2->posX;
    t1->posY = t2->posY;
    t2->posX = tempX;
    t2->posY = tempY;

	iter_swap(board.begin() + firstIndex, board.begin() + secondIndex);
}

bool Game::checkOrder() {
    int val1;
    int val2;
    for(int i=1; i<size*size-1; i++){
        val1 = board.at(i-1)->getValue();
        val2 = board.at(i)->getValue();
        if(val2 != val1 + 1) return false;
    }
    return true;
}

void Game::updateMoves() {

    for(auto el : board) el->setMove(MOVES::NONE);

    if(emptyX-1 >= 0) getTile(emptyX - 1, emptyY)->setMove(MOVES::DOWN);

    if(emptyX + 1 < size) getTile(emptyX + 1, emptyY)->setMove(MOVES::UP);

    if(emptyY + 1 < size) getTile(emptyX, emptyY + 1)->setMove(MOVES::LEFT);

    if(emptyY - 1 >= 0) getTile(emptyX, emptyY - 1)->setMove(MOVES::RIGHT);
}

void Game::setEmpty(int x, int y){
    emptyX = x;
    emptyY = y;

}

void Game::randomizeBoard(int steps = 1) {
    srand (time(NULL));
    int dx;
    int dy;
    int choice;
    for(int i=1; i <= steps; i++){
        while(true){
            choice = rand() % 4 + 1;
            switch (choice) {
            //UP
            case 1:{
                dx = 0;
                dy = -1;
                break;
            }
            //DOWN
            case 2:{
                dx = 0;
                dy = 1;
                break;
            }
            //LEFT
            case 3:{
                dx = -1;
                dy = 0;
                break;
            }
            //RIGHT
            case 4:{
                dx = 1;
                dy = 0;
                break;
            }
            }
            if((emptyX + dx >= 0) && (emptyX + dx < size) &&
                    (emptyY + dy >= 0) && (emptyY + dy < size)){
                swapTiles(getTile(emptyX + dx, emptyY + dy), getTile(emptyX, emptyY));
                emptyX += dx;
                emptyY += dy;
                break;
            }
        }
    }
}

int Game::getNumberOfMoves(){
    return score.getNumberOfMoves();
}

int Game::getSize(){
    return size;
}

Score& Game::getScore(){
    return score;
}

std::vector<Tile*> Game::getBoard(){
    return board;
}


