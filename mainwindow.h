#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "GameSerializer.h"
#include "gamewindow.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

/**
*Class reprezenting Menu Page of the game
*/
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * Default  QT UI constructor
    */
    MainWindow(QWidget *parent = nullptr);
    /**
     * Default QT UI destructor
    */
    ~MainWindow();

private slots:

    /**
     * Slot mthod connected with loading image for a tile
    */
    void on_btn_loadImage_clicked();


    /**
     * Slot mthod connected with starign a game
    */
    void on_btn_startGame_clicked();


    /**
     * Slot mthod connected with loading a saved game
    */
    void on_btn_loadGame_clicked();

private:
    Ui::MainWindow *ui; /*!< QT UI */

    QString image_path; /*!< Path to the tile image */
    GameWindow* game; /*!< Current game */
};
#endif // MAINWINDOW_H
