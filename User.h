#pragma once
#include <string>

/**
*Class reprezenting player
*/
class User
{
public:
    /**
    *Default constructor of a player
    */
	User();

    /**
    *Constructor setting player name
    * @param  username
    */
	User(std::string);

    /**
     * Method for getting username
     * \return username
    */
	std::string getName() const;

private:
    std::string name; /*!< username */
};

