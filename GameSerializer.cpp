#include "GameSerializer.h"


void GameSerializer::save(GameWindow* window, QString name) {

    Game game = window->getGame();
    QString path = QDir::currentPath() + "\\save\\" + name + ".txt";
    QFile file(path);
     if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
         return;

     QTextStream out(&file);
     out << game.getSize() << "\n";
     out << QString::fromStdString(game.getScore().getPlayer().getName()) << "\n";
     out << game.elapsedSecs << "\n";
     out << game.getScore().getNumberOfMoves() << "\n";
     out << window->getImagePath() << "\n";
     for(auto el : game.getBoard()) out << el->getValue() << "\n";
}


void GameSerializer::load(GameWindow* gamewindow, QString path) {
    int size;
    int time;
    int moves;

    QString img;
    QString player;
    std::vector<int> values;
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;
    QTextStream in(&file);
    size = in.readLine().toInt();
    player = in.readLine();
    time = in.readLine().toInt();
    moves = in.readLine().toInt();
    img = in.readLine();
    for(int i=0; i<size*size; i++){
      values.push_back(in.readLine().toInt());
    }

//    qInfo() << size << " " << player << " " << time << " " << moves << "\n";
//    for(auto val : values) qInfo() << val;


    Game toSet(size, time, moves, player.toStdString(), &values); //moze trzeba pointer
    gamewindow->setGame(toSet);
    gamewindow->setGridSize(size);
    gamewindow->setImagePath(img);
    gamewindow->refreshLCD();
    gamewindow->createLabels();
}
