#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QMainWindow>
#include <vector>
#include <QLabel>
#include "Game.h"
#include <QPainter>
#include <QRectF>
#include <QMouseEvent>
#include <algorithm>
#include <QTimer>
#include <QElapsedTimer>
#include <QDateTime>
#include <QMessageBox>
#include <QInputDialog>
#include <QDir>





namespace Ui {
class GameWindow;
}

/**
*Class reprezenting window displayed in-game
*/
class GameWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * Constructor creating a game
     * @param QT Parent
     * @param board grid size
     * @param username
     * @param tile image path
    */
    GameWindow(QWidget *parengt = nullptr, int grid_size = 3, QString username = "unknown player", QString = QDir::currentPath());

    /**
     * Constructor creating a game window
     * @param QT Parent
     * @param tile image path
    */
    GameWindow(QWidget *parengt, QString = QDir::currentPath());

    /**
     * Default destructor
    */
    ~GameWindow();

    /**
     * Method used for setting Game for GameWindow object
     * @param Game object
    */
    void setGame(Game&);

    /**
     * Method used for creating image and text labels for board tiles
    */
    void createLabels();

    /**
     * Method used for getting Game object
     * \return Game object
    */
    Game& getGame();

    /**
     * Method used for getting grid size
     * @param size of the grid
    */
    void setGridSize(int size);

    /**
     * Method used for getting grid size
     * \return path
    */
    QString getImagePath();

    /**
     * Method used for setting image path
     * @param path
    */
    void setImagePath(QString);

    /**
     * Method used for refreshing displayed LCD time
    */
    void refreshLCD();


private slots:

    /**
     * Slot method for fetching coordinates of mouse click
     * Used for determining which Tile was clicked to move
     * @param mouse click event
    */
    void mousePressEvent(QMouseEvent*);

    /**
     * Slot method for swapping text and image labels
     * @param x postition of Tile
     * @param y position of Tile
    */
    void swapLabels(int, int);


    /**
     * Slot method for ending the game and displaying results
    */
    void endGame();

    /**
     * Slot method for setting timer when game starts
    */
    void setTimer();

    /**
     * Slot method for updating elapsed time
    */
    QString updateTime();

    /**
     * Slot method for saving current game into the file
    */
    void on_pushButton_clicked();

private:
    Ui::GameWindow *ui; /*!< QT UI */
    int grid_size = 3; /*!< grid size */
    std::vector<QLabel*> imgLabels; /*!< Vector storing labels with Tile images */
    std::vector<QLabel*> textLabels; /*!< Vector storing labels with Tile values text */
    Game game; /*!< Curernt Game object */
    QString image_path; /*!< Tile image path */

    int board_size = 700; /*!< Size of the entire board */
    int puzzle_size = 0; /*!< Size of the one Tile */
    int init_posX = 10; /*!< Initial position X of first Tile */
    int init_posY = 10; /*!< Initial position Y of first Tile */
    int offset = 1; /*!< Space between the tiles on the grid */


    QTimer* timer; /*!< Main timer */
    QElapsedTimer* elapsed; /*!< Main timer elapsed measure */
};


#endif // GAMEWINDOW_H
