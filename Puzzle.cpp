#include "Puzzle.h"

Puzzle::Puzzle(int x, int y, int v) : Tile(x,y){
	value = v;
    move = NONE;
}

int Puzzle::getValue() const {
	return value;
}

int Puzzle::getMove() const {
	return move;
}

void Puzzle::setMove(int direction){
    move = direction;
}
