#pragma once
#include "Tile.h"

/**
*Class reprezenting movable puzzle - not empty
*/
class Puzzle : public Tile
{
public:
    /**
    *Constructor calling base class constructor
    *and setting value and empty direction of a Puzzle
    *@param x postition on the grid
    *@param y postition on the grid
    *@param v value of the Puzzle
    */
	Puzzle(int, int, int);

    /**
     * Virtual method used to get value of a Puzzle
     * \return Puzzle value (order)
    */
    virtual int getValue() const;

    /**
     * Virtual method used to get available move of a Puzzle
     * \return Puzzle available move direction
    */
	virtual int getMove() const;

    /**
     * Virtual method used to set available move of a Puzzle
     * @param direction of move
    */
    virtual void setMove(int);

    int value; /*!< Assigned and stored value (order) */
    int move; /*!< Available direction to move*/
};

